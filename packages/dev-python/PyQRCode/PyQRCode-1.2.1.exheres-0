# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ]

SUMMARY="QR code generator with SVG, EPS, PNG and terminal output"
DESCRIPTION="
The pyqrcode module is a QR code generator that is simple to use and written in pure python. The
module can automates most of the building process for creating QR codes. Most codes can be created
using only two lines of code!

Unlike other generators, all of the helpers can be controlled manually. You are free to set any or
all of the properties of your QR code.

QR codes can be saved as SVG, PNG (by using the pypng module), and plain text. They can also be
displayed directly in most Linux terminal emulators. PIL is not used to render the image files.

The pyqrcode module attempts to follow the QR code standard as closely as possible. The terminology
and the encodings used in pyqrcode come directly from the standard. This module also follows the
algorithm laid out in the standard.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pypng[>=0.0.13][python_abis:*(-)?]
"

# Doesn't have tests, last checked: 1.2.1
src_test() {
    :
}

